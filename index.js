const arr = [ "Kyiv", ["Lva Tovstogo", "Pecherska"], "Dog Stitch", "Cupcake", "Ukrainian Salo",];

let list = document.createElement("ul");
document.body.append(list);
list.style.fontSize='20px';

function createList(array) {

	let newArr = array.map(el => {

		if (!Array.isArray(el)) {
			return `<li>${el}</li>`;
		} else {
			return `<ul>${createList(el)}</ul>`;
		}
	}).join('');

	return newArr;
}

list.innerHTML = createList(arr);
let timerBlock = document.createElement("div");
timerBlock.style.fontSize='30px';
timerBlock.style.fontWeight='900';
timerBlock.style.padding='50px 50px';
document.body.append(timerBlock);
let timeleft = 3;
timerBlock.innerText = 'Time: ';
let timerNumber = document.createElement("span");
timerNumber.innerText = `${timeleft}`;
timerBlock.append(timerNumber);
document.body.prepend(timerBlock);


let downloadTimer = setInterval( () =>{
     timeleft--;
     timeleft <= 0 ? clearInterval(downloadTimer) : timerNumber.innerHTML = `${timeleft}`;
}, 1000);

setTimeout(el => {
 	list.remove();
 	timerBlock.remove();
}, 3000);
